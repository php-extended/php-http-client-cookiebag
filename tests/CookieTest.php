<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-cookiebag library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\Cookie;
use PHPUnit\Framework\TestCase;

/**
 * CookieTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\Cookie
 *
 * @internal
 *
 * @small
 */
class CookieTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Cookie
	 */
	protected Cookie $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('=; SameSite=None', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Cookie();
	}
	
}
