<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-cookiebag library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Psr\Http\Message\RequestInterface;
use Stringable;

/**
 * Cookie class file.
 * 
 * This class represents a cookie according to RFC6265.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class Cookie implements Stringable
{
	
	public const SAME_SITE_NONE = 'None';
	public const SAME_SITE_LAX = 'Lax';
	public const SAME_SITE_STRICT = 'Strict';
	
	/**
	 * The name of the cookie.
	 * 
	 * @var ?string
	 */
	protected ?string $_name = null;
	
	/**
	 * The value of the cookie.
	 * 
	 * @var ?string
	 */
	protected ?string $_value = null;
	
	/**
	 * When this cookie expires.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_expires = null;
	
	/**
	 * When this cookies expires².
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_maxAge = null;
	
	/**
	 * The domain to which this cookie is limited to.
	 * 
	 * @var ?string
	 */
	protected ?string $_domain = null;
	
	/**
	 * The path to which this cookie is limited to.
	 * 
	 * @var ?string
	 */
	protected ?string $_path = null;
	
	/**
	 * The creation time of the cookie.
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_creationTime;
	
	/**
	 * The last access time of the cookie.
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_lastAccessTime;
	
	/**
	 * Whether this cookie is persistent.
	 * 
	 * @var boolean
	 */
	protected bool $_persistent = false;
	
	/**
	 * Whether this cookie is host-only.
	 * 
	 * @var boolean
	 */
	protected bool $_hostOnly = true;
	
	/**
	 * Whether this cookie should only be sent to secure connections.
	 * 
	 * @var boolean
	 */
	protected bool $_secure = false;
	
	/**
	 * Whether this cookie should only be sent through http(s) connections.
	 * 
	 * @var boolean
	 */
	protected bool $_httpOnly = false;
	
	/**
	 * The same site directive (rfc6265bis). ["Strict", "Lax", "None"].
	 * 
	 * @var string
	 */
	protected string $_sameSite = self::SAME_SITE_NONE;
	
	/**
	 * The extension of the cookie.
	 * 
	 * @var ?string
	 */
	protected ?string $_extension = null;
	
	/**
	 * Builds a new Cookie with the given initialized variables.
	 */
	public function __construct()
	{
		$this->_creationTime = new DateTimeImmutable();
		$this->_lastAccessTime = new DateTimeImmutable();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function __toString() : string
	{
		$str = $this->getNameValuePairStr();
		
		if(null !== $this->_expires)
		{
			$str .= '; Expires='.$this->_expires->format(DateTime::COOKIE);
		}
		
		if(null !== $this->_maxAge)
		{
			$str .= '; Max-Age='.((string) ($this->_maxAge->getTimestamp() - \time()));
		}
		
		if(null !== $this->_domain && '' !== $this->_domain)
		{
			$str .= '; Domain='.$this->_domain;
		}
		
		if(null !== $this->_path && '' !== $this->_path)
		{
			$str .= '; Path='.$this->_path;
		}
		
		if($this->_secure)
		{
			$str .= '; Secure';
		}
		
		if($this->_httpOnly)
		{
			$str .= '; HttpOnly';
		}
		
		$str .= '; SameSite='.$this->_sameSite;
		
		if(null !== $this->_extension && '' !== $this->_extension)
		{
			$str .= '; Extension='.$this->_extension;
		}
		
		return $str;
	}
	
	/**
	 * Gets an unique identifier for this cookie.
	 * 
	 * @return string
	 */
	public function getId() : string
	{
		return ((string) $this->getDomain())
			.'/'.\trim((string) $this->getPath(), '/')
			.':'.((string) $this->getName());
	}
	
	/**
	 * Gets the name of the cookie.
	 * 
	 * @return ?string
	 */
	public function getName() : ?string
	{
		return $this->_name;
	}
	
	/**
	 * Sets the name of the cookie.
	 * 
	 * @param string $name
	 */
	public function setName(string $name) : void
	{
		$this->_name = $name;
	}
	
	/**
	 * Gets the value of the cookie.
	 * 
	 * @return ?string
	 */
	public function getValue() : ?string
	{
		return $this->_value;
	}
	
	/**
	 * Sets the value of the cookie.
	 * 
	 * @param string $value
	 */
	public function setValue(string $value) : void
	{
		$this->_value = $value;
	}
	
	/**
	 * Gets when this cookie expires.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getExpires() : ?DateTimeInterface
	{
		return $this->_expires;
	}
	
	/**
	 * Sets when this cookie expires.
	 * 
	 * @param DateTimeInterface $expires
	 */
	public function setExpires(DateTimeInterface $expires) : void
	{
		$this->_expires = $expires;
	}
	
	/**
	 * Gets when this cookie expires.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getMaxAge() : ?DateTimeInterface
	{
		return $this->_maxAge;
	}
	
	/**
	 * Sets when this cookie expires.
	 * 
	 * @param DateTimeInterface $maxAge
	 */
	public function setMaxAge(DateTimeInterface $maxAge) : void
	{
		$this->_maxAge = $maxAge;
	}
	
	/**
	 * Gets the domain this cookie is limited to.
	 * 
	 * @return ?string
	 */
	public function getDomain() : ?string
	{
		return $this->_domain;
	}
	
	/**
	 * Sets the domain this cookie is limited to.
	 * 
	 * @param string $domain
	 */
	public function setDomain(string $domain) : void
	{
		$this->_domain = (string) \mb_strtolower($domain);
	}
	
	/**
	 * Gets the path this cookie is limited to.
	 * 
	 * @return ?string
	 */
	public function getPath() : ?string
	{
		return $this->_path;
	}
	
	/**
	 * Sets the path this cookie is limited to.
	 * 
	 * @param string $path
	 */
	public function setPath(string $path) : void
	{
		$this->_path = $path;
		if(empty($this->_path))
		{
			$this->_path = '/';
		}
	}
	
	/**
	 * Gets the creation time for this cookie.
	 * 
	 * @return DateTimeInterface
	 */
	public function getCreationTime() : DateTimeInterface
	{
		return $this->_creationTime;
	}
	
	/**
	 * Updates the creation time of this cookie by the creation time of the
	 * other cookie.
	 * 
	 * @param Cookie $other
	 */
	public function recoverCreationTime(Cookie $other) : void
	{
		if($this->_creationTime->getTimestamp() > $other->_creationTime->getTimestamp())
		{
			$this->_creationTime = $other->_creationTime;
		}
	}
	
	/**
	 * Gets the last access time for this cookie.
	 * 
	 * @return DateTimeInterface
	 */
	public function getLastAccessTime() : DateTimeInterface
	{
		return $this->_lastAccessTime;
	}
	
	/**
	 * Updates the last access time to current time.
	 */
	public function updateLastAccessTime() : void
	{
		$this->_lastAccessTime = new DateTimeImmutable();
	}
	
	/**
	 * Gets whether this cookie is persistent.
	 * 
	 * @return boolean
	 */
	public function isPersistent() : bool
	{
		return $this->_persistent;
	}
	
	/**
	 * Sets the persistent flag.
	 * 
	 * @param boolean $persistent
	 */
	public function setPersistent(bool $persistent) : void
	{
		$this->_persistent = $persistent;
	}
	
	/**
	 * Gets whether this cookie is host only.
	 * 
	 * @return boolean
	 */
	public function isHostOnly() : bool
	{
		return $this->_hostOnly;
	}
	
	/**
	 * Sets the host only flag.
	 * 
	 * @param boolean $hostOnly
	 */
	public function setHostOnly(bool $hostOnly) : void
	{
		$this->_hostOnly = $hostOnly;
	}
	
	/**
	 * Gets whether this cookie should only be sent to secure connections.
	 * 
	 * @return boolean
	 */
	public function isSecure() : bool
	{
		return $this->_secure;
	}
	
	/**
	 * Sets whether this cookie should only be sent to secure connections.
	 * 
	 * @param boolean $secure
	 */
	public function setSecure(bool $secure) : void
	{
		$this->_secure = $secure;
	}
	
	/**
	 * Gets whether this cookie should only be sent by http(s) protocols.
	 * 
	 * @return boolean
	 */
	public function isHttpOnly() : bool
	{
		return $this->_httpOnly;
	}
	
	/**
	 * Sets whether this cookie should only be send by http(s) protocols.
	 * 
	 * @param boolean $httpOnly
	 */
	public function setHttpOnly(bool $httpOnly) : void
	{
		$this->_httpOnly = $httpOnly;
	}
	
	/**
	 * Sets the same site directive of the cookie.
	 * 
	 * @param string $sameSiteValue
	 */
	public function setSameSite(string $sameSiteValue) : void
	{
		switch(\mb_strtolower($sameSiteValue))
		{
			case 'strict':
				$this->_sameSite = self::SAME_SITE_STRICT;
				break;
			
			case 'lax':
				$this->_sameSite = self::SAME_SITE_LAX;
				break;
			
			case 'none':
				$this->_sameSite = self::SAME_SITE_NONE;
		}
	}
	
	/**
	 * Gets the same site directive.
	 * 
	 * @return string
	 */
	public function getSameSite() : string
	{
		return $this->_sameSite;
	}
	
	/**
	 * Gets the extension of the cookie.
	 * 
	 * @return ?string
	 */
	public function getExtension() : ?string
	{
		return $this->_extension;
	}
	
	/**
	 * Sets the extension of the cookie.
	 * 
	 * @param string $extension
	 */
	public function setExtension(string $extension) : void
	{
		$this->_extension = $extension;
	}
	
	/**
	 * Gets whether this cookie matches this given request. Returns true if it
	 * matches (i.e. it should be included with it), and false if it does not
	 * (i.e. it should not be included with it).
	 *  
	 * @param RequestInterface $request
	 * @return boolean
	 */
	public function matches(RequestInterface $request) : bool
	{
		return $this->expireMatches()
			&& $this->maxAgeMatches()
			&& $this->domainMatches($request->getUri()->getHost())
			&& $this->pathMatches($request->getUri()->getPath())
			&& $this->secureMatches($request->getUri()->getScheme())
			&& $this->httpOnlyMatches($request->getUri()->getScheme())
			&& $this->sameSiteMatches($request->getUri()->getHost());
	}
	
	/**
	 * Gets the string RFC-compliant version of the cookie for Cookie headers.
	 * 
	 * @return string
	 */
	public function getNameValuePairStr() : string
	{
		return ((string) $this->_name).'='.((string) $this->_value);
	}
	
	/**
	 * Returns true if the expires data is still valid, else false.
	 * 
	 * @return boolean
	 */
	protected function expireMatches() : bool
	{
		return null === $this->_expires 
			|| $this->_expires->getTimestamp() > \time();
	}
	
	/**
	 * Returns true if the max age data is still valid, else false.
	 * 
	 * @return boolean
	 */
	protected function maxAgeMatches() : bool
	{
		return null === $this->_maxAge
			|| $this->_maxAge->getTimestamp() > \time();
	}
	
	/**
	 * Returns true if the domain of the cookie is included into the domain
	 * of the request, else false.
	 * 
	 * @param string $hostname
	 * @return boolean
	 */
	protected function domainMatches(string $hostname) : bool
	{
		if(null === $this->_domain)
		{
			return true;
		}
		
		// TODO domain canonicalization with punycode
		$includes = $this->getDomainParts($this->_domain);
		$recipients = $this->getDomainParts($hostname);
		
		if(\count($includes) > \count($recipients))
		{
			return false;
		}
		
		if($this->_hostOnly && \count($recipients) > \count($includes))
		{
			return false;
		}
		
		for($i = 0; \count($includes) > $i; $i++)
		{
			if(isset($includes[$i], $recipients[$i]) && $includes[$i] !== $recipients[$i])
			{
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Gets the parts of a domain in right order.
	 *
	 * @param string $domain
	 * @return array<integer, string>
	 */
	protected function getDomainParts(string $domain) : array
	{
		// TODO domain canonicalization with punycode
		// array map : remove trailing spaces
		// array filter : remove empty domain shards
		// array reverse : ensure the domain are from less generic (tld) to most generic
		$parts = [];
		
		foreach(\array_map('trim', \explode('.', (string) \mb_strtolower($domain))) as $part)
		{
			if(!empty($part))
			{
				$parts[] = $part;
			}
		}
		
		return \array_reverse($parts);
	}
	
	/**
	 * Returns true if the path of the cookie is included into the path of
	 * the request, else false.
	 * 
	 * @param string $path
	 * @return boolean
	 */
	protected function pathMatches(string $path) : bool
	{
		return null === $this->_path 
			|| 0 === \mb_strpos($path, $this->_path);
	}
	
	/**
	 * Returns true if the scheme of the request is allowed by the secure
	 * setting, else false.
	 * 
	 * @param string $scheme
	 * @return boolean
	 */
	protected function secureMatches(string $scheme) : bool
	{
		if(!$this->_secure)
		{
			return true;
		}
		
		return (bool) \preg_match('#^[a-z]+s$#', (string) \mb_strtolower($scheme));
	}
	
	/**
	 * Returns true if the scheme of the request is allowed by the http only
	 * setting, else false.
	 * 
	 * @param string $scheme
	 * @return boolean
	 */
	protected function httpOnlyMatches(string $scheme) : bool
	{
		if(!$this->_httpOnly)
		{
			return true;
		}
		
		return false !== \mb_strpos((string) \mb_strtolower($scheme), 'http');
	}
	
	/**
	 * Returns true if the same site.
	 * 
	 * @param string $domain
	 * @return boolean
	 */
	protected function sameSiteMatches(string $domain) : bool
	{
		if(self::SAME_SITE_NONE === $this->_sameSite)
		{
			return true;
		}
		
		if(null === $this->_domain)
		{
			return false;
		}
		
		if(self::SAME_SITE_LAX === $this->_sameSite)
		{
			return false !== \mb_strpos((string) \mb_strtolower($domain), $this->_domain);
		}
		
		if(self::SAME_SITE_STRICT === $this->_sameSite)
		{
			return 0 === \strcasecmp($domain, $this->_domain);
		}
		
		return false;
	}
	
}
