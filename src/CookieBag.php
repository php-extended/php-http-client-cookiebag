<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-cookiebag library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use InvalidArgumentException;
use PhpExtended\Tld\TopLevelDomainHierarchyInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;
use Stringable;

/**
 * CookieBag class file.
 * 
 * This class manages all the existing cookies, adds and removes them when
 * necessary, collects them from responses and adds them to appropriate
 * requests.
 * 
 * @see https://tools.ietf.org/html/rfc6265
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class CookieBag implements Stringable
{
	
	/**
	 * The tld hierarchy.
	 * 
	 * @var TopLevelDomainHierarchyInterface
	 */
	protected TopLevelDomainHierarchyInterface $_tldHierarchy;
	
	/**
	 * The configuration for this bag.
	 *
	 * @var CookieBagConfiguration
	 */
	protected CookieBagConfiguration $_configuration;
	
	/**
	 * All the cookies that are currently available and collected.
	 * 
	 * @var array<string, Cookie>
	 */
	protected array $_cookies = [];
	
	/**
	 * Builds a new CookieBag with the given settings.
	 * 
	 * @param TopLevelDomainHierarchyInterface $tldHierarchy
	 * @param CookieBagConfiguration $configuration
	 */
	public function __construct(TopLevelDomainHierarchyInterface $tldHierarchy, CookieBagConfiguration $configuration)
	{
		$this->_tldHierarchy = $tldHierarchy;
		$this->_configuration = $configuration;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Adds the appropriate cookie to the specific request, and returns the
	 * newly created request with the cookies added.
	 * 
	 * @param RequestInterface $request
	 * @return RequestInterface
	 */
	public function addCookies(RequestInterface $request) : RequestInterface
	{
		if($this->_configuration->shouldReplaceInRequests())
		{
			$request = $request->withoutHeader('Cookie');
		}
		
		foreach($this->_cookies as $cookie)
		{
			if($cookie->matches($request))
			{
				try
				{
					$request = $request->withAddedHeader('Cookie', $cookie->getNameValuePairStr());
					$cookie->updateLastAccessTime();
				}
				catch(InvalidArgumentException $ec)
				{
					// ignore
				}
			}
		}
		
		return $request;
	}
	
	/**
	 * Gathers the set-cookies from the given response and adds them to the
	 * persistant list of managed cookies.
	 * 
	 * @param ResponseInterface $response
	 * @return ResponseInterface
	 */
	public function collectCookies(ResponseInterface $response) : ResponseInterface
	{
		// newer header set by native client
		$targetUris = $response->getHeader('X-Request-Uri');
		if(empty($targetUris))
		{
			// older header set by native client
			$targetUris = $response->getHeader('X-Target-Uri');
			if(empty($targetUris))
			{
				// as we cannot ensure that the website does not tries to set
				// cookies for other domains that itself, we deny the whole
				// possibility to set cookies
				return $response;
			}
		}
		$targetUri = (string) \array_pop($targetUris); // get the last one
		$hostname = (string) \parse_url($targetUri, \PHP_URL_HOST);
		$hostpath = (string) \parse_url($targetUri, \PHP_URL_PATH);
		if(empty($hostname))
		{
			return $response;
		}
		
		$cookieFactory = new CookieFactory();
		
		foreach($response->getHeader('Set-Cookie') as $cookieHeader)
		{
			try
			{
				$cookie = $cookieFactory->parse($hostname, $hostpath, $cookieHeader);
			}
			catch(RuntimeException $exc)
			{
				// on failure, just ignore the cookie
				continue;
			}
			$this->handleNewCookie($hostname, $cookie);
		}
		if($this->_configuration->shouldRemoveInResponses())
		{
			$response = $response->withoutHeader('Set-Cookie');
		}
		
		// do garbage collection if needed
		$this->checkGc();
		
		return $response;
	}
	
	/**
	 * Handles the given cookie as to be added to the list of known cookies.
	 * This method replaces existing cookies by this one whenever needed, and
	 * ensure that this cookie is a legitimate cookie to keep.
	 * 
	 * @param string $hostname
	 * @param Cookie $cookie
	 * @return boolean whether the cookie was effectively added to the list
	 */
	protected function handleNewCookie(string $hostname, Cookie $cookie) : bool
	{
		$cid = $cookie->getId();
		
		// if the cookie is expired, then reject it and cleanup just in case
		$timestamp = \time();
		$expires = $cookie->getExpires();
		if(null !== $expires && $expires->getTimestamp() < $timestamp)
		{
			unset($this->_cookies[$cid]);
			
			return false;
		}
		
		$maxAge = $cookie->getMaxAge();
		if(null !== $maxAge && $maxAge->getTimestamp() < $timestamp)
		{
			unset($this->_cookies[$cid]);
			
			return false;
		}
		
		if(isset($this->_cookies[$cid]))
		{
			// if the cookie already exists, then it is legitimate to replace
			// it when the server asks for it
			$cookie->recoverCreationTime($this->_cookies[$cid]);
			$this->_cookies[$cid] = $cookie;
			
			return true;
		}
		
		// if the domain of the request do not correspond to the domain of the
		// cookie, then we deny the cookie
		if(!$this->isDomainIncluded((string) $cookie->getDomain(), $hostname))
		{
			return false;
		}
		
		// if the domain is in the suffix list, then the cookie is illegitimate
		// thus, we do not store it.
		$isTld = $this->_tldHierarchy->isTld($cookie->getDomain());
		if($isTld)
		{
			return false;
		}
		
		// finally, store the cookie
		$this->_cookies[$cid] = $cookie;
		
		return true;
	}
	
	/**
	 * Gets whether the domainToInclude is included into the domainRecipient.
	 * 
	 * As per RFC6265, 'example.com' and 'foo.example.com' are included in
	 * 		'foo.example.com',
	 * but 'bar.example.com' and 'baz.foo.example.com' are not included in
	 * 		'foo.example.com'
	 * 
	 * @param string $domainToInclude
	 * @param string $domainRecipient
	 * @return boolean
	 */
	protected function isDomainIncluded(string $domainToInclude, string $domainRecipient) : bool
	{
		$includes = $this->getDomainParts($domainToInclude);
		$recipients = $this->getDomainParts($domainRecipient);
		
		if(\count($includes) > \count($recipients))
		{
			return false;
		}
		
		for($i = 0; \count($includes) > $i; $i++)
		{
			if(isset($includes[$i], $recipients[$i]) && $includes[$i] !== $recipients[$i])
			{
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Gets the parts of a domain in right order.
	 * 
	 * @param string $domain
	 * @return array<integer, string>
	 */
	protected function getDomainParts(string $domain) : array
	{
		// TODO domain canonicalization with punycode
		// array map : remove trailing spaces
		// array filter : remove empty domain shards
		// array reverse : ensure the domain are from less generic (tld) to most generic
		$parts = [];
		
		foreach(\array_map('trim', \explode('.', (string) \mb_strtolower($domain))) as $part)
		{
			if(!empty($part))
			{
				$parts[] = $part;
			}
		}
		
		return \array_reverse($parts);
	}
	
	/**
	 * Checks whether the garbage collection should be done. If so, does it.
	 */
	protected function checkGc() : void
	{
		$int = 1 + ((int) \mt_rand(0, 100));
		if($this->_configuration->getGcProbability() < $int)
		{
			$this->doGc();
		}
	}
	
	/**
	 * Does the garbage collection. This will remove expired cookies, then
	 * cookies that share a domain field with more than allowed other cookies, 
	 * then older cookies until we get an acceptable number of cookies.
	 * 
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	protected function doGc() : void
	{
		$timestamp = \time();
		
		foreach($this->_cookies as $id => $cookie)
		{
			$expires = $cookie->getExpires();
			if(null !== $expires && $expires->getTimestamp() < $timestamp)
			{
				unset($this->_cookies[$id]);
			}
			
			$maxAge = $cookie->getMaxAge();
			if(null !== $maxAge && $maxAge->getTimestamp() < $timestamp)
			{
				unset($this->_cookies[$id]);
			}
		}
		
		if(\count($this->_cookies) > $this->_configuration->getQuantitySoftLimit())
		{
			// begin the hunt by domain
			$domains = [];
			
			foreach($this->_cookies as $cookie)
			{
				$domain = (string) $cookie->getDomain();
				if(isset($domains[$domain]))
				{
					$domains[$domain]++;
					continue;
				}
				$domains[$domain] = 1;
			}
			
			foreach($domains as $domain => $quantity)
			{
				if($this->_configuration->getQuantityLinkedLimit() < $quantity)
				{
					// remove older cookies until the limit is ok
					$cookies = $this->getCookiesOrderedByLastAccess($domain);
					$qtyToRemove = $quantity - $this->_configuration->getQuantityLinkedLimit();
					$qty = 0;
					
					foreach($cookies as $cookieSet)
					{
						foreach($cookieSet as $cookie)
						{
							unset($this->_cookies[$cookie->getId()]);
							if($qty >= $qtyToRemove)
							{
								break;
							}
							$qty++;
						}
					}
				}
			}
		}
		
		if(\count($this->_cookies) > $this->_configuration->getQuantitySoftLimit())
		{
			$qtyToRemove = \count($this->_cookies) - $this->_configuration->getQuantitySoftLimit();
			// remove older cookies until the limit is ok
			$cookies = $this->getCookiesOrderedByLastAccess();
			$qty = 0;
			
			foreach($cookies as $cookieSet)
			{
				foreach($cookieSet as $cookie)
				{
					unset($this->_cookies[$cookie->getId()]);
					if($qty >= $qtyToRemove)
					{
						break;
					}
					$qty++;
				}
			}
		}
		
		\gc_collect_cycles();
	}
	
	/**
	 * Gets all the cookies belonging to given domain (or all cookies if no
	 * domain is given), ordered by date of last access.
	 * 
	 * @param string $domain
	 * @return array<integer, array<integer, Cookie>>
	 */
	protected function getCookiesOrderedByLastAccess($domain = null) : array
	{
		$cookies = [];
		
		foreach($this->_cookies as $cookie)
		{
			$timestamp = $cookie->getLastAccessTime()->getTimestamp();
			if(null === $domain || $this->isDomainIncluded((string) $cookie->getDomain(), $domain))
			{
				if(isset($cookies[$timestamp]))
				{
					$cookies[$timestamp][] = $cookie;
					continue;
				}
				$cookies[$timestamp] = [$cookie];
			}
		}
		// ksort : sort by timestamp increasing, meaning older first
		\ksort($cookies);
		
		return $cookies;
	}
	
}
