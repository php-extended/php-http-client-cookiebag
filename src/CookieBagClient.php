<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-cookiebag library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use PhpExtended\Tld\TopLevelDomainHierarchyInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Stringable;

/**
 * CookieBagClient class file.
 * 
 * This class is an implementation of a client which does handle cookies
 * that are sent by the server and is able to send them back to the server
 * according to the target urls and other cookie parameters.
 * 
 * @author Anastaszor
 */
class CookieBagClient implements ClientInterface, Stringable
{
	
	/**
	 * The inner client.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_client;
	
	/**
	 * The inner cookie bag.
	 * 
	 * @var CookieBag
	 */
	protected CookieBag $_cookieBag;
	
	/**
	 * Builds a new CookieBagClient with the given inner client.
	 * 
	 * @param ClientInterface $client
	 * @param TopLevelDomainHierarchyInterface $tldHierarchy
	 * @param CookieBagConfiguration $configuration
	 */
	public function __construct(ClientInterface $client, TopLevelDomainHierarchyInterface $tldHierarchy, ?CookieBagConfiguration $configuration = null)
	{
		$this->_client = $client;
		if(null === $configuration)
		{
			$configuration = new CookieBagConfiguration();
		}
		$this->_cookieBag = new CookieBag($tldHierarchy, $configuration);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Client\ClientInterface::sendRequest()
	 */
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		$request = $this->_cookieBag->addCookies($request);
		
		$response = $this->_client->sendRequest($request);
		
		return $this->_cookieBag->collectCookies($response);
	}
	
}
