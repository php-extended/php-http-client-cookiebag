<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-cookiebag library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Stringable;

/**
 * CookieBagConfiguration class file.
 * 
 * This class represents the configuration for the cookie bag.
 * 
 * @author Anastaszor
 */
class CookieBagConfiguration implements Stringable
{
	
	/**
	 * Whether to replace cookies that may already exists in the request, and
	 * only provides those that are available in this bag. Defaults to false,
	 * and keep previous cookies on the request.
	 * 
	 * @var boolean
	 */
	protected bool $_replaceInRequests = false;
	
	/**
	 * Whether to remove cookies that are present in the response, and gets only
	 * pass the other headers for the response. Defaults to false, meaning the
	 * response will keep all of its cookie headers.
	 * 
	 * @var boolean
	 */
	protected bool $_removeInResponses = false;
	
	/**
	 * The quantity of cookies to hold before triggering any garbage collection.
	 * 
	 * @var integer
	 */
	protected int $_quantitySoftLimit = 3000;
	
	/**
	 * The quantity of cookies that may shared a domain before triggering any
	 * garbage collection.
	 * 
	 * @var integer
	 */
	protected int $_quantityLinkedLimit = 50;
	
	/**
	 * The probability that garbage collection amongst cookies is triggered.
	 * Defaults to 10%.
	 * 
	 * @var integer
	 */
	protected int $_gcProbability = 10;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Enables the replacement of request cookies.
	 */
	public function enableReplaceInRequests() : void
	{
		$this->_replaceInRequests = true;
	}
	
	/**
	 * Disables the replacement of request cookies.
	 */
	public function disableReplaceInRequests() : void
	{
		$this->_replaceInRequests = false;
	}
	
	/**
	 * Gets whether to replace request cookies.
	 * 
	 * @return boolean
	 */
	public function shouldReplaceInRequests() : bool
	{
		return $this->_replaceInRequests;
	}
	
	/**
	 * Enables the removal of response cookies.
	 */
	public function enableRemoveInResponses() : void
	{
		$this->_removeInResponses = true;
	}
	
	/**
	 * Disables the removal of response cookies.
	 */
	public function disableRemoveInResponses() : void
	{
		$this->_removeInResponses = false;
	}
	
	/**
	 * Gets whether to remove response cookies.
	 * 
	 * @return boolean
	 */
	public function shouldRemoveInResponses() : bool
	{
		return $this->_removeInResponses;
	}
	
	/**
	 * Sets the quantity soft limit.
	 * 
	 * @param integer $value
	 */
	public function setQuantitySoftLimit(int $value) : void
	{
		if(0 >= $value)
		{
			return;
		}
		$this->_quantitySoftLimit = $value;
	}
	
	/**
	 * Gets the quantity soft limit.
	 * 
	 * @return integer
	 */
	public function getQuantitySoftLimit() : int
	{
		return $this->_quantitySoftLimit;
	}
	
	/**
	 * Sets the quantity linked limit.
	 * 
	 * @param integer $value
	 */
	public function setQuantityLinkedLimit(int $value) : void
	{
		if(0 >= $value)
		{
			return;
		}
		$this->_quantityLinkedLimit = $value;
	}
	
	/**
	 * Gets the quantity linked limit.
	 * 
	 * @return integer
	 */
	public function getQuantityLinkedLimit() : int
	{
		return $this->_quantityLinkedLimit;
	}
	
	/**
	 * Sets the garbage collector probability (0-100, outside range values are
	 * cropped within the range).
	 * 
	 * @param integer $value
	 */
	public function setGcProbability(int $value) : void
	{
		if(0 >= $value || 100 <= $value)
		{
			return;
		}
		$this->_gcProbability = $value;
	}
	
	/**
	 * Gets the garbage collector probability.
	 * 
	 * @return integer
	 */
	public function getGcProbability() : int
	{
		return $this->_gcProbability;
	}
	
}
