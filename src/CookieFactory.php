<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-cookiebag library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use DateInterval;
use DateTime;
use DateTimeImmutable;
use RuntimeException;
use Stringable;

/**
 * CookieFactory class file.
 * 
 * This class parses the cookie strings and create cookies according to RFC6265.
 * 
 * @author Anastaszor
 */
class CookieFactory implements Stringable
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Parses the given string and creates a new cookie.
	 * 
	 * @param string $requestHostname
	 * @param string $requestHostpath
	 * @param string $cookieString
	 * @return Cookie
	 * @throws RuntimeException if the cookie cannot be parsed
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function parse(string $requestHostname, string $requestHostpath, string $cookieString) : Cookie
	{
		$values = \array_map('trim', \explode(';', $cookieString));
		$first = true;
		$cookie = new Cookie();
		
		foreach($values as $keyPair)
		{
			$data = \array_map('trim', \explode('=', $keyPair, 2));
			if($first)
			{
				if(!isset($data[1]))
				{
					throw new RuntimeException(\strtr('Failed to find value for cookie {name}', ['{name}' => $data[0]]));
				}
				if('' === $data[0])
				{
					throw new RuntimeException(\strtr('Failed to find name for cookie {value}', ['{value}' => $data[1]]));
				}
				$cookie->setName($data[0]);
				$cookie->setValue($data[1]);
				$first = false;
				continue;
			}
			
			switch(\mb_strtolower($data[0]))
			{
				case 'expires':
					// just ignore expires without values
					if(!isset($data[1]))
					{
						break;
					}
					$dti = DateTimeImmutable::createFromFormat(DateTime::COOKIE, $data[1]);
					// just ignore expires with wrong date format
					if(false === $dti)
					{
						break;
					}
					$cookie->setExpires($dti);
					$cookie->setPersistent(true);
					break;
				
				case 'max-age':
					// just ignore max-age without values
					if(!isset($data[1]) || !\is_numeric($data[1]))
					{
						break;
					}
					$dti = new DateTimeImmutable();
					$data[1] = (int) $data[1];
					/** @var DateInterval $interval */
					$interval = DateInterval::createFromDateString('+ '.((string) $data[1]).' seconds');
					$dti = $dti->add($interval);
					$cookie->setMaxAge($dti);
					$cookie->setPersistent(true);
					break;
				
				case 'domain':
					// just ignore cookies with wrong domain values
					if(!isset($data[1]) || empty($data[1]))
					{
						break;
					}
					// if the domain begins or ends by a ".", just ignore it 
					$data[1] = \trim($data[1], '.');
					$cookie->setDomain($data[1]);
					$cookie->setHostOnly(false);
					break;
				
				case 'path':
					// just ignore cookies with wrong path values
					if(!isset($data[1]) || empty($data[1]))
					{
						break;
					}
					$cookie->setPath($data[1]);
					break;
				
				case 'secure':
					$cookie->setSecure(true);
					break;
				
				case 'httponly':
					$cookie->setHttpOnly(true);
					break;
				
				case 'samesite':
					if(isset($data[1]))
					{
						$cookie->setSameSite($data[1]);
					}
					break;
				
				case 'extension':
					// just ignore cookies with wrong extension values
					if(!isset($data[1]) || empty($data[1]))
					{
						break;
					}
					$cookie->setExtension($data[1]);
					break;
					
				default:
					// just ignore other values
			}
		}
		
		if($cookie->getDomain() === null || $cookie->getDomain() === '')
		{
			$cookie->setDomain($requestHostname);
		}
		
		if($cookie->getPath() === null || $cookie->getPath() === '')
		{
			$cookie->setPath($requestHostpath);
		}
		
		return $cookie;
	}
	
}
