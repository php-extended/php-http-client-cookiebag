# php-extended/php-http-client-cookiebag
A psr-18 compliant middleware client that handles cookies for psr-7 http messages.

![coverage](https://gitlab.com/php-extended/php-http-client-cookiebag/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-http-client-cookiebag/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-http-client-cookiebag ^8`


## Basic Usage

This library is to make a man in the middle for http requests and responses
and logs the events when requests passes. It may be used the following way :

```php

/* @var $client Psr\Http\Client\ClientInterface */    // psr-18
/* @var $request Psr\Http\Message\RequestInterface */ // psr-7

$client = new CookieBagClient($client);
$response = $client->sendRequest($request);

/* @var $response Psr\Http\Message\ResponseInterface */

```

This library handles the adding of `Cookie` header on the requests and the
decoding according to the `Set-Cookie` headers on the responses.


## License

MIT (See [license file](LICENSE)).
